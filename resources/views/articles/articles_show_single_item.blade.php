@extends('applayout')

@section('main_content')

	<a href='{{url("articles")}}'><button class="btn btn-primary">Back to list of articles</button></a>

	<h2>{{ $article->title }}</h2>
	<p>{{ $article->content }}</p>

	<a href='{{url("articles/$article->id/delete")}}'><button class="btn btn-danger">Delete</button></a>

	<h3>Post a new comment</h3>
	<form action="" method="POST">
		{{ csrf_field() }}
		Comment: <br>
		<textarea name="description"></textarea><br>
		<input type="submit">
	</form>

	<h3>Comments</h3>
	<ul>
		@foreach($article->comments as $comment)
		<li>
			{{ $comment->description}}
		</li>
		@endforeach
	</ul>
@endsection