@extends('applayout')

@section('main_content')

<h2>
	<a href='{{url("articles/create")}}'><button class="btn btn-success">Create a new article</button></a>
</h2>

<h2>Current Preference: {{ $preference }} </h2>
<form action='{{url("setpreference")}}' method="POST">
	{{ csrf_field() }}
	Preference:
	<select name="preference">
		<option value="politics">Politics</option>
		<option value="weather">Weather</option>
		<option value="sports">Sports</option>
	</select>
	<input type="submit" value="Set">
</form>




<h3> List of Articles </h3>
<ul>
	@foreach($all_articles as $article)
	<li>
		<a href= '{{url( "articles/$article->id" )}}'> {{ $article-> title }} </a> 
	</li>
	@endforeach
</ul>


@endsection