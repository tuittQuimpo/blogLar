@extends('applayout')

@section('main_content')


	<a href="{{url('articles')}}"><button class="btn btn-warning">Back to Articles</button></a>

	@if (count($errors) > 0)
		<ul>
			@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>
	@endif

	<h1>
		Create a new article
	</h1>

		<div class="container">
		  <form method="POST">
		  	{{ csrf_field() }}

		    <div class="form-group">
		      <label>Title:</label>
		      <input type="text" class="form-control" placeholder="Enter title" name="title">
		    </div>

		   <div class="form-group">
			  <label>Content:</label>
			  <textarea class="form-control" rows="5" name="content"></textarea>
		   </div>
		 
		    <button type="submit" class="btn btn-info">Submit</button>
		  </form>
		</div>

@endsection
