<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class ArticlesController extends Controller
{
    function showArticles(Request $request){
    	$all_articles = \App\Article::all();
    	$preference = $request ->session()->get('preference', 'default_preference');
    	return view('articles.articles_list', compact('all_articles', 'preference'));
    }
    
    function show($id){
	$article = \App\Article::find($id);
	$result = $article->comments;
    // $related_comments = $article->comments()->get();
    // dd($related_comments);

    return view('articles.articles_show_single_item',
	 compact('article'));

    }

    function create(){
    	return view('articles.articles_create');
    }
    function store(Request $request){
    	$title = $request->get('title');
    	$content = $request->get('content');

    	$rules = array(
    		'title' => 'required | min:3 | max:10 | alpha_num', 
    		'content' => 'required'
    	);
    	$this->validate($request,$rules);

    	$article_obj = new \App\Article();
    	$article_obj->title = $title;
    	$article_obj->content = $content;
    	$article_obj->save();

    	return redirect("articles");
    }

    function delete($id){
    	$article_to_delete = \App\Article::find($id);
    	$article_to_delete->delete();

    	return redirect("articles");
    }

    function setPreference(Request $request){
    	$preference = $request->get('preference');

    	$request->session()->put('preference',$preference);

    	return redirect("articles");
    }

    function postComment(Request $request,$id){
        $comment = $request->description;
        $user_id = Auth::user()->id;
        $article_id = $id;

        $comment_obj = new \App\Comment;
        $comment_obj->user_id = $user_id;
        $comment_obj->article_id = $article_id;
        $comment_obj->description = $comment;
        $comment_obj->save();

        return redirect("articles/$id");
    }
}

